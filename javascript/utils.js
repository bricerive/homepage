var statusTimeout = 0;

function setStatus(theText) {
  window.status = theText;
  if (theBrowser.canOnMouseOut == false) {
    clearTimeout(statusTimeout);
    statusTimeout = setTimeout('clearStatus()', 5000);
  }
  return true;
}

function clearStatus() {
  if (window != null) window.status = '';
}

function BrowserInfo() {
  this.code = 'unknown';
  this.version = 0;
  this.platform = 'Win';

  var i = navigator.userAgent.indexOf('MSIE');
  if (i >= 0) {
    this.code = 'MSIE';
    this.version = parseFloat(navigator.userAgent.substring(i+5, i+9));
  } else {
    i = navigator.userAgent.indexOf('Mozilla/')
    if (i >= 0) {
      this.code = 'NS';
      this.version = parseFloat(navigator.userAgent.substring(i+8, i+12));
    }
  }
  if (navigator.userAgent.indexOf('Mac') >= 0) {this.platform = 'Mac';}
  if (navigator.userAgent.indexOf('OS/2') >= 0) {this.platform = 'OS/2';}
  if (navigator.userAgent.indexOf('X11') >= 0) {this.platform = 'UNIX';}

  this.canOnMouseOut = ((this.code == 'NS') && (this.version >= 3));
  this.hasRentrantBug = ((this.code == 'NS') && (this.version == 2.01) && (this.platform != 'Win'));
  this.mustMoveAfterLoad = ((this.code == 'NS') && (this.version >= 3));
}

var theBrowser = null;
theBrowser = new BrowserInfo;

function TheDate(what) {
  update = new Date(what.lastModified);
  theDate = update.getDate();
  theMonth = update.getMonth() + 1;
  theYear = update.getYear();
  return '<font size=1>Last update: '+theDate+'/'+theMonth+'/'+theYear+'</font>';
}

function testCookies() { 
	var exp = new Date(); 
	exp.setTime(exp.getTime() + 1800000); 
	// first write a test cookie 
	setCookie("cookies", "cookies", exp, false, false, false); 
	if (parent.cookie.indexOf('cookies') == -1) { 
		alert("This site's navigation needs cookies to work properly"); 
	}
	//else alert("Got Cookies!");
	// now delete the test cookie 
	exp = new Date(); 
	exp.setTime(exp.getTime() - 1800000); 
	setCookie("cookies", "cookies", exp, false, false, false); 
}

function setCookie(name, value, expires, path, domain, secure) { 
	var curCookie = name + "=" + escape(value) + 
		((expires) ? "; expires=" + expires.toGMTString() : "") + 
		((path) ? "; path=" + path : "") + 
		((domain) ? "; domain=" + domain : "") + 
		((secure) ? "; secure" : ""); 
	parent.cookie = curCookie; 
}

function DoTracking () {
  for (i = 0; i < parent.frames.length; i++) {
    if (parent.frames[i].name == "code") {
      parent.frames[i].theMenu.TrackUpdate();
      return;
    }
  }
  self.location.href="../index.html";
}

function SetCrtPage() {
  testCookies();
  path = self.location.pathname;
  parent.cookie = "crtPage="+path;
}