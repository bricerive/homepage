
//--------------------------------------------------------------
// Class ImgStoreItem

function ImgStoreItem(theName, theSrc, w, h)
{
  this.name = theName;
  this.src = theSrc;
  this.w = w;
  this.h = h;
  this.Set = ImgStoreItemSet;
  this.GetTag = ImgStoreItemGetTag;
}

function ImgStoreItemSet(theName, theSrc, theWidth, theHeight)
{with(this){
  name = theName;
  src = theSrc;
  w = theWidth;
  h = theHeight;
}}

function ImgStoreItemGetTag()
{with (this){
  return '<img src="'+src+'" width='+w+' height='+h+' border=0 align="top">';
}}

//--------------------------------------------------------------
// Class ImgStore

function ImgStore()
{
  this.count = -1;
  this.item = new ImgStoreItem("","",0,0);
  this.Find = ImgStoreFind;
  this.Add = ImgStoreAdd;
  this.GetTag = ImgStoreGetTag;
}

function ImgStoreFind(theName)
{with (this) {
    for (var i=0; i<=count; i++)
      if (item[i].name == theName)
        return i;
    return -1;
}}

function ImgStoreAdd(theName, theSrc, theWidth, theHeight)
{with(this){
  if (theSrc) {
    var i = Find(theName);
		if (i == -1)
      item[++count] = new ImgStoreItem(theName, theSrc, theWidth, theHeight);
		else
      item[i].Set(theName, theSrc, theWidth, theHeight);
  }
}}

function ImgStoreGetTag(theName)
{with(this){
  var i = Find(theName);
  if (i==0) return '';
  return item[i].GetTag();
}}

//--------------------------------------------------------------
// Menu item object
function MenuItem (parent, id, type, text, url, comment)
{
  this.parent = parent;
  this.id = id;
  this.type = type;
  this.text = text;
  this.url = url;
  this.comment = comment;

  this.prevItem = -1;
  this.nextItem = -1;
  this.lastKid = -1;
  this.firstKid = -1;
  
  this.isopen = false;
  
  this.IsFolder = MenuItemIsFolder;
  this.Draw = MenuItemDraw;
  this.IconName = MenuItemIconName;
  this.SetIsOpen = MenuItemSetIsOpen;
  this.ImgName = MenuItemImgName;
  this.MouseOver= MenuItemMouseOver;
  this.MouseOut = MenuItemMouseOut;
}

function MenuItemIsFolder()
{with(this){
	if (type=='Folder')
		return true;
	return false;
}}

function MenuItemImgName ()
{with(this){
  if (type == 'Folder' && isopen)
    return 'FolderOpen';
  if (type == 'Document' && isopen)
    return 'DocumentOpen';
  return type;
}}

function MenuItemDraw(indentStr)
{with(this){
	var me = '"return parent.code.theMenu.entry[' + id + '].';
	var iconTag = store.GetTag(IconName());
	var aLine = '<nobr>' + indentStr;
	if (IsFolder()) {
		aLine += '<A HREF="javascript:parent.code.theMenu.Toggle('+id+')"';
		aLine += 'onFocus="if(this.blur)this.blur()"'; // Removes boxing of selected URL
		aLine += 'onMouseOver=' + me + 'MouseOver(\'plusMinusIcon\');" ';
		aLine += 'onMouseOut=' + me + 'MouseOut(\'plusMinusIcon\');">' + iconTag + '</A>';
		aLine += '<A HREF="javascript:parent.code.theMenu.Toggle(' + id + ')"';
		aLine += 'title="' + comment + '" ';
		aLine += 'onFocus="if(this.blur)this.blur()"'; // Removes boxing of selected URL
		aLine += 'onMouseOver=' + me + 'MouseOver(\'docIcon\');" ';
		aLine += 'onMouseOut=' + me + 'MouseOut(\'docIcon\');">';
		aLine += store.GetTag(ImgName());
		aLine += text;
		aLine += '</a>';
	} else {
		aLine += iconTag;
		if (isopen) {
			aLine += '<span class="text" >'
			aLine += store.GetTag(ImgName());
			aLine += text;
			aLine += '</span>';
		} else {
			aLine += '<A HREF="'+baseDir+'Pages/'+url+'" ';
			aLine += 'TARGET="text" ';
			aLine += 'onFocus="if(this.blur)this.blur()" '; // Removes boxing of selected URL
			aLine += 'title="' + comment + '" ';
			aLine += 'onMouseOver=' + me + 'MouseOver(\'docIcon\');" ';
			aLine += 'onMouseOut=' + me + 'MouseOut(\'docIcon\');">';
			aLine += store.GetTag(ImgName());
			aLine += text;
			aLine += '</a>';
		}
	}
	aLine += '</nobr><br>';
	return aLine
}}

function MenuItemIconName()
{with(this){
  var iconName = 'icon' + ((firstKid != -1) ? ((isopen == true) ? 'Minus' : 'Plus') : 'Join');
  iconName += (id == 0) ? ((nextItem == -1) ? 'Only' : 'Top') : ((nextItem == -1) ? 'Bottom' : '');
  return iconName;
}}

function MenuItemSetIsOpen (isOpen)
{with(this){
  if (isopen == isOpen) return false;
  isopen = isOpen;
  return true;
}}

function MenuItemMouseOver(imgName)
{with(this){
  //parent.menuFrame.status = '';  //Needed for setStatus to work on MSIE 3 - Go figure!?
  if (imgName == 'plusMinusIcon')
    setStatus('Click to ' + ((isopen == true) ? 'collapse.' : 'expand.'));
  else
    setStatus(comment);
  return true;
}}

function MenuItemMouseOut(imgName)
{with(this){
  clearStatus();
  return true;
}}

//--------------------------------------------------------------
// The Menu object.
// This is basically an array object although the data in it is a tree.
function Menu ()
{  
	this.menuTop = 40;
	this.entryHeight = 16;
	this.styleSheet="menu.css";
	this.topImage="";
	this.backgroundImage="";
	
	this.entry = new MenuItem;
	this.entry[0] = new MenuItem(-1,0,"Folder","Root","","");
	this.count = 0;
	this.current = -1;
	
	this.AddItem = MenuAddItem;
	
	this.Draw = MenuDraw;
	this.DrawLevel = MenuDrawLevel;
	
	this.Toggle = MenuToggle;
	this.OpenAll = MenuOpenAll;
	this.OpenBranch = MenuOpenBranch;

	this.TrackUpdate = MenuTrackUpdate;
	this.FindEntry = MenuFindEntry;
	this.Select = MenuSelect;
		
	this.SetNav = MenuSetNav;
	this.ToggleNav = MenuToggleNav;
	this.SetOs = MenuSetOs;
	this.ToggleOs = MenuToggleOs;
	this.ToggleLanguage = MenuToggleLanguage;
}

function MenuAddItem(parentId, type, text, url, comment)
{with(this){
	//alert("MenuAddItem("+parentId+", "+type+", "+text+", "+url+", "+comment+")");
	// Create the new item
	var index=++count;
	entry[index] = new MenuItem(parentId,index,type,text,url,comment);
	// Add it to the tree
	var parent = entry[parentId];
	entry[index].prevItem = parent.lastKid;
	if (parent.lastKid>=0) entry[parent.lastKid].nextItem = index;
	if (parent.lastKid<0) parent.firstKid=index;
	parent.lastKid = index;
	return index;
}}

function MenuDraw()
{with(this){
	//alert("MenuDraw()");
	parent.menuFrame.document.close();
	parent.menuFrame.document.open();
	parent.menuFrame.document.writeln('<HTML><HEAD><TITLE></TITLE><link rel="stylesheet" href="' + baseDir + styleSheet + '" type="text/css"></HEAD>');
	parent.menuFrame.document.writeln('<body background="'+baseDir+backgroundImage+'">');
	parent.menuFrame.document.writeln('<img src="' +baseDir+ topImage + '" border=0" + height="'+baseDir+topImageH+'"><br>');
	parent.menuFrame.document.writeln('<img src="'+baseDir+'../images/dot_clear.gif" border=0" + height="'+menuTop+'"><br>');
	parent.menuFrame.document.writeln('<table width="180" border="0" cellspacing="0" cellpadding="0">');
	var width = 180 - menuLeft;
	parent.menuFrame.document.writeln('<tr><td width="' + menuLeft + '"></td><td>');
	DrawLevel(1, ''); // Skip the root
	parent.menuFrame.document.writeln('</td></tr></table>');
	parent.menuFrame.document.writeln('</BODY></HTML>');
	parent.menuFrame.document.close();
}}

function MenuDrawLevel(index, indentStr)
{with(this){
	//alert("MenuDrawLevel("+index+", "+indentStr+")");
	var currEntry = index;
	var tempStr = "";
	var aLine = "";
	var e = null;
	while (index > -1) {
		e = entry[index];
		aLine = e.Draw(indentStr);
		parent.menuFrame.document.writeln(aLine);
		if (e.firstKid > -1) {
			if (e.isopen) {
				tempStr = (e.nextItem == -1) ? 'iconBlank' : 'iconJoinOnly';
				DrawLevel(e.firstKid, indentStr + store.GetTag(tempStr));
			}
		}
		index = e.nextItem;
	}
}}

function MenuSelect(index)
{with(this){
	if (index!=current) {
		if (current>=0)
			entry[current].SetIsOpen(false);
		current=index;
		var e=entry[index];
		if (e.IsFolder()) {
			alert("Cannot select a folder");
			return;
		}
		e.SetIsOpen(true);
		OpenBranch(e.parent, true);
		Draw();
	}	
}}

function MenuFindEntry(theURL)
{with(this){
	for (var i = 0; i <= count; i++)
	if (entry[i].url == theURL)
		return i;
	return -1;
}}

function MenuTrackUpdate()
{with(this){
	var path = parent.text.location.pathname;
	var slash = path.lastIndexOf('/');
	if (slash!=-1)
		path = path.substring(slash+1,path.length);
	var index = FindEntry(path);
	//alert("MenuTrackUpdate: "+path+"="+index+"<->"+current);
	if (index!=-1)
		Select(index);
}}

function MenuToggle(index)
{with(this){
	var e = entry[index];
	if (e.IsFolder())
		if (e.SetIsOpen(e.isopen? false: true))
			Draw();
}}

function MenuOpenAll(state)
{with(this){
	var hasChanged = false;
	for (var i = 0; i <= count; i++) {
		var e = entry[i];
		if (e.firstKid != -1)
			hasChanged = e.SetIsOpen(state) || hasChanged;
	}
	if (hasChanged) Draw();
}}

function MenuOpenBranch(index, state)
{with(this){
	var toRefresh=false;
	while (index>=0) {
		var f=entry[index];
		if (!f.IsFolder()) {
			alert("Cannot open documents");
			return;
		}
		toRefresh = toRefresh || f.SetIsOpen(state);
		index = f.parent;
	}
	return toRefresh;
}}

function MenuSetNav(navId)
{with(this){
  //if (gTrace) alert("MenuSetNav "+navId);
  var navName = "NS";
  if (navId!=0) navName="MSIE";
  if (theBrowser.code != navName) {
    theBrowser.code = navName;
    initOutlineIcons();
    Draw();
  }
}}

function MenuToggleNav()
{with(this){
  //if (gTrace) alert("MenuToggleNav");
  if (theBrowser.code == "NS") SetNav(1);
  else SetNav(0);
}}

function MenuSetOs(osId)
{with(this){
 //if (gTrace) alert("MenuSetOs "+osId);
  var osName = "Mac";
  if (osId!=0) osName="Win";
  if (theBrowser.platform != osName) {
    theBrowser.platform = osName;
    initOutlineIcons();
    Draw();
  }
}}

function MenuToggleOs()
{with(this){
  if (theBrowser.platform == "Mac") SetOs(1);
  else SetOs(0);
}}

function MenuToggleLanguage()
{with(this){
  //alert("MenuToggleLanguage");

	cook=parent.cookie;
	if (cook && cook.length>8 && cook.substring(0,7) == "crtPage") {
		cookLength = cook.length;
		crtLang = cook.substring(cookLength-6, cookLength-5);
		var crtDoc;
		if (crtLang=='F')
			crtDoc = cook.substr(0, cookLength-6);
		else
			crtDoc = cook.substr(0, cookLength-5);
		if (crtLang=="F") {
			parent.cookie = crtDoc+".html";
	 //	alert("parent.cookie="+parent.cookie);
			parent.location="index.html";
		} else {
			parent.cookie = crtDoc+"F.html";
	 	//alert("parent.cookie="+parent.cookie);
			parent.location="indexF.html";
		}
	}
}}

//--------------------------------------------------------------
function initOutlineIcons()
{
  //if (gTrace) alert("initOutlineIcons");
  var h = theMenu.entryHeight;
  var w = (18.0 * h) / 16.0;
  var ip = baseDir+'../images/menu/';
  ip += (theBrowser.platform == 'Mac') ? 'mac/' : 'win/';
  store.Add('iconPlusTop', ip + 'plustop.gif', w, h);
  store.Add('iconPlus', ip + 'plus.gif', w, h);
  store.Add('iconPlusBottom', ip + 'plusbottom.gif', w, h);
  store.Add('iconPlusOnly', ip + 'plusonly.gif', w, h);
  store.Add('iconMinusTop', ip + 'minustop.gif', w, h);
  store.Add('iconMinus', ip + 'minus.gif', w, h);
  store.Add('iconMinusBottom', ip + 'minusbottom.gif', w, h);
  store.Add('iconMinusOnly', ip + 'minusonly.gif', w, h);
  store.Add('iconJoin', ip + 'join.gif', w, h);
  store.Add('iconJoinTop', ip + 'jointop.gif', w, h);
  store.Add('iconJoinBottom', ip + 'joinbottom.gif', w, h);
  store.Add('iconJoinOnly', ip + 'line.gif', w, h);
  store.Add('iconBlank', ip + 'blank.gif', w, h);

  // Add folder and document images to the imgStore.
  store.Add('Folder', ip + 'folderclosed.gif', w, h);
  store.Add('FolderOpen', ip + 'folderopen.gif', w, h);
  var di = ip + ((theBrowser.code == 'NS') ? 'document2.gif' : 'document.gif');
  store.Add('Document', di, w, h);
	var di2 = ip + ((theBrowser.code == 'NS') ? 'document2O.gif' : 'documentO.gif');
	store.Add('DocumentOpen', di2, w, h);
}

// Set to null to help the browser clean up memory. 
var theMenu = null;
var store = null;
theMenu = new Menu;
store = new ImgStore();

var baseDir = document.location.href;
baseDir = baseDir.substring(0, baseDir.lastIndexOf('/')) + '/';


